//
//  ShopImage.swift
//  JeenchShop
//
//  Created by IvanLazarev on 02/08/2017.
//  Copyright © 2017 IvanLazarev. All rights reserved.
//

import UIKit
import CoreData


extension ShopImage {
    public var image: UIImage? {
        get {
            guard let data = imageData, let image = UIImage(data: data as Data) else {
                return nil
            }
            return image
        }
        set {
            if let image = newValue, let data = UIImageJPEGRepresentation(image, 1.0) {
                imageData = data as NSData
            } else {
                imageData = nil
            }
        }
    }
}
