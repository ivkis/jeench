//
//  ImageCell.swift
//  JeenchShop
//
//  Created by IvanLazarev on 02/08/2017.
//  Copyright © 2017 IvanLazarev. All rights reserved.
//

import UIKit


class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
