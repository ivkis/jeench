//
//  AddShopViewController.swift
//  JeenchShop
//
//  Created by IvanLazarev on 02/08/2017.
//  Copyright © 2017 IvanLazarev. All rights reserved.
//

import UIKit


class AddShopViewController: UIViewController {

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var shopNameTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var buildingTextField: UITextField!

    var images = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField()

        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture))
        longPressGesture.minimumPressDuration = 0.3
        collectionView.addGestureRecognizer(longPressGesture)
    }

    // MARK: - Actions

    @IBAction func saveButtonTapped(_ sender: Any) {
        guard validateForm() else {
            return
        }
        let address = "\(countryTextField.text!), \(cityTextField.text!), \(streetTextField.text!), \(buildingTextField.text!)"
        let googleGeoposition = GoogleAPI()
        saveButton.isEnabled = false
        googleGeoposition.forwardGeocoding(address: address) { location in
            self.saveButton.isEnabled = true
            if let location = location {
                DataStorage.shared.addShop(name: self.shopNameTextField.text!, images: self.images, latitude: location.latitude, longitude: location.longitude)
                self.navigationController?.popViewController(animated: true)
            } else {
                self.showError(text: "Failed to parse address. Please, check provided information.")
            }
        }
    }

    @IBAction func addPhotoButtonTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Source photos", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.chooseImagePickerAction(source: .camera)
        })
        let photoLibAction = UIAlertAction(title: "Library", style: .default, handler: { (action) in
            self.chooseImagePickerAction(source: .photoLibrary)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }

    func chooseImagePickerAction(source: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    func setupTextField() {
        // Handle keyboard "Next" button
        shopNameTextField.addTarget(countryTextField, action: #selector(UIView.becomeFirstResponder), for: .editingDidEndOnExit)
        countryTextField.addTarget(cityTextField, action: #selector(UIView.becomeFirstResponder), for: .editingDidEndOnExit)
        cityTextField.addTarget(streetTextField, action: #selector(UIView.becomeFirstResponder), for: .editingDidEndOnExit)
        streetTextField.addTarget(buildingTextField, action: #selector(UIView.becomeFirstResponder), for: .editingDidEndOnExit)
        buildingTextField.addTarget(buildingTextField, action: #selector(UIView.resignFirstResponder), for: .editingDidEndOnExit)
    }

    func validateForm() -> Bool {
        for field in [shopNameTextField, countryTextField, cityTextField, streetTextField, buildingTextField] {
            if field?.text?.isEmpty != false {
                field?.becomeFirstResponder()
                showError(text: "Please fill all fields")
                return false
            }
        }
        return true
    }

    func showError(text: String) {
        let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        let alertController = UIAlertController(title: "", message: text, preferredStyle: .alert)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func handleLongGesture(gesture: UILongPressGestureRecognizer) {

        switch(gesture.state) {

        case UIGestureRecognizerState.began:
            guard let selectedIndexPath = collectionView.indexPathForItem(at: gesture.location(in: collectionView)) else {
                break
            }
            collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case UIGestureRecognizerState.changed:
            collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case UIGestureRecognizerState.ended:
            collectionView.endInteractiveMovement()
        default:
            collectionView.cancelInteractiveMovement()
        }
    }
}


extension AddShopViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            images.append(image)
            collectionView.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}


extension AddShopViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let image = images[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.imageCell, for: indexPath)!
        cell.imageView.image = image
        return cell
    }
}


extension AddShopViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        images.insert(images.remove(at: sourceIndexPath.row), at: destinationIndexPath.row)
    }
}
