//
//  ShowOnTheMapViewController.swift
//  JeenchShop
//
//  Created by IvanLazarev on 02/08/2017.
//  Copyright © 2017 IvanLazarev. All rights reserved.
//

import UIKit
import GoogleMaps


class ShowOnTheMapViewController: UIViewController {
    
    override func loadView() {
        let camera = GMSCameraPosition.camera(withLatitude: 43.905, longitude: 42.728, zoom: 12.0)
        let mapView = GMSMapView.map(withFrame: UIScreen.main.bounds, camera: camera)
        mapView.isMyLocationEnabled = true
        view = mapView

        var bounds = GMSCoordinateBounds()
        for shop in DataStorage.shared.shops {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: shop.latitude, longitude: shop.longitude)
            marker.title = shop.name
            marker.map = mapView
            bounds = bounds.includingCoordinate(marker.position)
        }

        if DataStorage.shared.shops.count == 1 {
            mapView.moveCamera(GMSCameraUpdate.setTarget(bounds.northEast))

        } else if DataStorage.shared.shops.count > 1 {
            mapView.moveCamera(GMSCameraUpdate.fit(bounds, withPadding: 60))
        }
    }
}
