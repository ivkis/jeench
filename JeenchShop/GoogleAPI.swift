//
//  GoogleAPI.swift
//  JeenchShop
//
//  Created by IvanLazarev on 02/08/2017.
//  Copyright © 2017 IvanLazarev. All rights reserved.
//

import Foundation
import CoreLocation


class GoogleAPI {

    func forwardGeocoding(address: String, callback: @escaping (CLLocationCoordinate2D?) -> Void) {
        var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
        let key = URLQueryItem(name: "key", value: "AIzaSyCDA5NqsAopqWn5F8ZU1Rd2WRFcnTXCMR0")
        let address = URLQueryItem(name: "address", value: address)
        components.queryItems = [key, address]

        let task = URLSession.shared.dataTask(with: components.url!) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, error == nil else {
                    print("Forward geocoding - failed to get response: " + String(describing: response))
                    print("Error: " + String(describing: error))
                    callback(nil)
                    return
                }

                guard let json = try! JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                    print("Forward geocoding: non-json response")
                    print("Response: " + (String(data: data, encoding: .utf8) ?? "Not string?!?"))
                    callback(nil)
                    return
                }

                guard let results = json["results"] as? [[String: Any]],
                    let status = json["status"] as? String,
                    status == "OK" else {
                        print("Forward geocoding: no results")
                        print("Json: " + String(describing: json))
                        callback(nil)
                        return
                }

                if let geometry = results.first?["geometry"] as? [String: Any],
                        let location = geometry["location"] as? [String: CLLocationDegrees],
                        let lat = location["lat"], let lng = location["lng"] {
                    let location2d = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    callback(location2d)
                } else {
                    print("Forward geocoding: failed to parse results")
                    print("Json: " + String(describing: json))
                    callback(nil)
                }
            }
        }
        task.resume()
    }
}
