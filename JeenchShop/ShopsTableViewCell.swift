//
//  ShopsTableViewCell.swift
//  JeenchShop
//
//  Created by IvanLazarev on 02/08/2017.
//  Copyright © 2017 IvanLazarev. All rights reserved.
//

import UIKit

class ShopsTableViewCell: UITableViewCell {

    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var nameLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(with shop: Shop) {
        shopImageView.image = (shop.images?.firstObject as? ShopImage)?.image
        nameLable.text = shop.name
        shopImageView.layer .cornerRadius = 35
        shopImageView.clipsToBounds = true
    }
}
