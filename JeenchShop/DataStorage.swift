//
//  DataStorage.swift
//  JeenchShop
//
//  Created by IvanLazarev on 02/08/2017.
//  Copyright © 2017 IvanLazarev. All rights reserved.
//

import UIKit
import CoreData


class DataStorage {

    static let shared = DataStorage()
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private(set) var shops = [Shop]()

    init() {
        downloadFromCoreData()
    }

    func addShop(name: String, images: [UIImage], latitude: Double, longitude: Double) {
        let entity = NSEntityDescription.entity(forEntityName: "Shop", in: context)
        let taskObject = NSManagedObject(entity: entity!, insertInto: context) as! Shop
        taskObject.name = name
        taskObject.latitude = latitude
        taskObject.longitude = longitude
        shops.insert(taskObject, at: 0)
        for image in images {
            let entity = NSEntityDescription.entity(forEntityName: "ShopImage", in: context)
            let shopImage = NSManagedObject(entity: entity!, insertInto: context) as! ShopImage
            shopImage.image = image
            taskObject.addToImages(shopImage)
        }
        saveArrayInCoreData()
    }

    func deleteShop(at index: Int) {
        let shopToDelete = shops[index]
        context.delete(shopToDelete)
        do {
            try context.save()
            shops.remove(at: index)
        } catch let error as NSError {
            print("Error: \(error), description \(error.userInfo)")
        }
    }

    func saveContext(context: NSManagedObjectContext) {
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }

    func saveArrayInCoreData() {
        for (index, shop) in shops.enumerated() {
            shop.positionShop = Int64(index)
        }
        saveContext(context: context)
    }

    func downloadFromCoreData() {
        let fetchRequest: NSFetchRequest<Shop> = Shop.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "positionShop", ascending: true)]

        do {
            shops = try context.fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>) as! [Shop]
        } catch {
            print(error.localizedDescription)
        }
    }
}
